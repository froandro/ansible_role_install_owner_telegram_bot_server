Ansible_role_install_owner_telegram_bot_server
=========

Роль устанавливает Ваш собственный сервер `telegram-bot-api` из готового образа `aiogram/telegram-bot-api:latest`

Requirements
------------

- система на базе Ubuntu
- telegram `api-id`
- telegram `api-hash`

requirements.txt:
```
# from GitLab or other git-based scm
- src: https://gitlab.com/froandro/ansible_role_install_docker.git
  scm: git
```


Role Variables
--------------

Переменные определены в файле defaults/main.yml:
```
image: aiogram/telegram-bot-api:latest 	# скомпилированный образ telegram-bot-api
path_to_docker: /usr/bin/docker 	# каталог размещения установленного docker на целевом хосте 
volume_dir: telegram-bot-api 		# volume docker
api_id: < api-id > 			# собственный telegram api-id
api_hash: < api-hash >			# собственный api-hash

```

Example Playbook
----------------

Пример плейбука применительно к хостам группы `Ubuntu`:
```
    - hosts: Ubuntu
      roles:
         - ansible_role_install_owner_telegram_bot_server 
```

License
-------

BSD

Author Information
------------------
AlZa 2022
